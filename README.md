# ms_summary_generator

#### 介绍
Mindspore Summary Generator

#### 软件说明
用于测试

#### 安装教程
该代码配置的proto等可以直接运行，无需更改。如果需要更新这部分代码，可以：
1.  先clone mindinsight的代码，需要用到里面的模块。地址：https://gitee.com/mindspore/mindinsight.git
2.  cd ms_summary_generator
3.  将log_generators、proto_files和crc32.py复制到该目录
```shell
cp -r path-to-mindinsight/mindinsight/datavisual/proto_files/
cp -r path-to-mindinsight/tests/utils/log_generators ./
cp path-to-mindinsight/tests/utils/crc32.py ./
```
4. 修改一下这几个模块的路径：搜索替换“mindinsight.datavisual.”为“”（空）

#### 使用说明
可选参数--summary_dir，--step_num，--tag_num
```shell
python run_log_generator.py --summary_dir "./summary"
```

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

#### 说明
图片目录的图均来自https://pixabay.com/zh/
该网站已授权使用：
可以做商业用途
不要求署名

#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
