import json
import os
import time

from conf import IMAGE_DIR
from log_generators.graph_log_generator import GraphLogGenerator
from log_generators.images_log_generator import ImagesLogGenerator
from log_generators.scalars_log_generator import ScalarsLogGenerator

image_dir = None
if os.path.exists(IMAGE_DIR):
    image_dir = IMAGE_DIR

generators = {
    'graph': GraphLogGenerator(),
    'image': ImagesLogGenerator(image_dir),
    'scalar': ScalarsLogGenerator()
}


def generate_for_step(file_path, plugin, tags, step):
    for tag in tags:
        generators.get(plugin).generate_log(file_path, [step], tag)


def generate_log(summary_dir, plugins, tags, steps, file_name=None):
    if not os.path.exists(summary_dir):
        os.mkdir(summary_dir)

    if file_name is None:
        file_name = f"events.summary.{int(time.time())}.create_by_summary_generator"
    file_path = os.path.join(summary_dir, file_name)

    if 'graph' in plugins:
        graph_base_path = "log_generators/graph_base.json"
        with open(graph_base_path, 'r') as f:
            graph_dict = json.load(f)
        generators.get('graph').generate_log(file_path, graph_dict)

    for step in steps:
        for plugin in ['image', 'scalar']:
            if plugin in plugins:
                generate_for_step(file_path, plugin, tags, step)
